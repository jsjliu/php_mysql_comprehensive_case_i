<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>
            用户管理
        </title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="format-detection" content="telephone=no">
        <link rel="stylesheet" href="./css/x-admin.css" media="all">
        <script type="text/javascript" src="loginjs/jquery-1.11.1.min.js"></script>
    </head>
    <body>
        <div class="x-body">
            <blockquote class="layui-elem-quote">
                您好<span id="MyName">9999</span>，欢迎使用用户管理<span class="f-14">v1.0</span>
            </blockquote>
        </div>
        <script src="./lib/layui/layui.js" charset="utf-8"></script>
        <script src="./js/x-admin.js"></script>
        <script>
        var _hmt = _hmt || [];
        (function() {
          var hm = document.createElement("script");
          hm.src = "https://hm.baidu.com/hm.js?b393d153aeb26b46e9431fabaf0f6190";
          var s = document.getElementsByTagName("script")[0]; 
          s.parentNode.insertBefore(hm, s);
        })();
        //判断用户session
         var username = sessionStorage.getItem('username');
         if (username==""||username==null||username==undefined) {;}else{
            $("#MyName").html(username); 
         }
        </script>
    </body>
</html>