<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>
            用户管理
        </title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="format-detection" content="telephone=no">
        <link rel="stylesheet" href="./css/x-admin.css" media="all">
        <script type="text/javascript" src="loginjs/jquery-1.11.1.min.js"></script>
    </head>
    <body>
        <div class="x-body">
            <blockquote class="layui-elem-quote">
                您好<span id="MyName">9999</span>，欢迎来到个人中心
                <button style="height: 30px;border:0px; border-radius: 3px; padding: 3px; cursor: pointer; background: #2cba7c;color: #fff" onclick="Edit()">编辑个人信息</button>
            </blockquote>
        </div>
        <script src="./lib/layui/layui.js" charset="utf-8"></script>
        <script src="./js/x-layui.js" charset="utf-8"></script>
        <script>
            function getQueryString(name) {
               var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
               var r = window.location.search.substr(1).match(reg);
               if(r != null) {
                     return decodeURIComponent(r[2]);
               }
               return '';
           }   
            layui.use(['laydate','element','laypage','layer'], function(){
                $ = layui.jquery;//jquery
              laydate = layui.laydate;//日期插件
              lement = layui.element();//面包导航
              layer = layui.layer;//弹出层
            });
        //判断用户session
         var username = sessionStorage.getItem('username');
         if (username==""||username==null||username==undefined) {;}else{
            $("#MyName").html(username); 
         }
         //编辑用户
            function Edit(){
                var Id=getQueryString("Id");
                x_admin_show("编辑","member-edit.php?Id="+Id,400,600);
            }
        </script>
    </body>
</html>