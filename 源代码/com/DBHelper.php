<?php
    header("Content-Type: text/html;charset=utf-8");
   class DBHelper{
    private $mysqli;
    private static $host='127.0.0.1';
    private static $user='root';
    private static $pwd='root';
    private static $dbname='demo1';
    
    //构造方法-初始化数据库链接
    public function __construct(){
        $this->mysqli=new mysqli(self::$host,self::$user,self::$pwd,self::$dbname)
        or die('数据库链接出错:'.$this->mysqli->connect_error);    
        //设置数据库编码为utf8
        $this->mysqli->query('set names utf8');    
    }    


    //取总数
    public function GetTotal($sql){
        $result = $this->mysqli->query($sql) or die($this->mysqli->error);
        return $result->num_rows;
     }

    //查询 --将查询结果以标准json字符串的形式返回
    public function QuerySQL($sql){
        $arr=array();
        $result=$this->mysqli->query($sql) or die($this->mysqli->error);
        if($result){
        while($row=$result->fetch_assoc()){
            //将查询结果封装到一个数组中，返回给方法调用处
            $arr[]=$row;
        }    
        //释放查询结果资源
        $result->free();
        }    
        return json_encode($arr);
    }
    
    //分页查询  pageNo页数  pageSize页大小
    public function PageQuerySQL($sql,$pageNo,$pageSize){
        $arr=array();
        $sql=$sql." limit ".($pageNo-1)*$pageSize.",".$pageSize;
        $result=$this->mysqli->query($sql) or die($this->mysqli->error);
        if($result){
        while($row=$result->fetch_assoc()){
            //将查询结果封装到一个数组中，返回给方法调用处
            $arr[]=$row;
        }    
        //释放查询结果资源
        $result->free();
        }    
        return json_encode($arr);
    }

    //非查询的SQL语句 update、delete、insert等
    public function ExecSQL($sql){
        $result=$this->mysqli->query($sql) or die($this->mysqli->error);
        if(!$result){
        return 500;//表示操作失败    
        }else{
        if($this->mysqli->affected_rows>0){
            return 200;//操作成功    
        }else{
            return 400;//没有受影响的行    
        }
        }
    }
    }
?>