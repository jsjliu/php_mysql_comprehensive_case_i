<!DOCTYPE html>
<html>
<head>
<title>login</title>
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="loginjs/jquery-1.11.1.min.js"></script>
<link href="logincss/style.css" rel="stylesheet" type="text/css" media="all" />
</head>
<body>

<div class="main">
	<h1>综合案例一</h1>
	<div class="w3_login">
		<div class="w3_login_module">
			<div class="module form-module">
				<div class="toggle">
					<i class="fa fa-times fa-pencil"></i>
					<div class="tooltip">点击注册</div>
				</div>
				<div class="form">
					<h2>登录账号</h2>
					<form id="LoginForm" method="post">
						<input type="text" name="login_Username" placeholder="用户名" required=" " />
						<input type="password" name="login_Password" placeholder="密码" required=" " />
						<input type="submit" onclick="DoLogin()" value="登录" />
					</form>
				</div>
				<div class="form">
					<h2>注册</h2>
					<form id="RegestForm">
						<input type="text" id="username" name="username" placeholder="用户名" required=" " />
						<input type="text" id="sex" name="sex" placeholder="性别" required=" " />
						<input type="text" id="password" name="password" placeholder="密码" required=" " />
						<input type="text" id="Email" name="Email" placeholder="Email" required=" " />
						<input type="text" id="phone" name="phone" placeholder="电话" required=" " />
						<input type="text" id="address" name="address" placeholder="地址" required=" " />
						<input type="submit" onclick="DoRegest()" value="注册" />
					</form>
				</div>
				<div class="cta"></div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
sessionStorage.clear();
$('.toggle').click(function(){
	$(this).children('i').toggleClass('fa-pencil');
	$('.form').animate({height: "toggle",'padding-top': 'toggle','padding-bottom': 'toggle',opacity: "toggle"}, "slow");
});
//处理用户登录
function DoLogin (){ 
	//异步处理用户登录
	$.ajax({
            url: "action/CheckLogin.php", 
            dataType: "json", 
            async: true,
            data: $('#LoginForm').serialize(),//将比表单的值序列化作为参数提交 
            type: "POST", //请求方式
            success: function (req) {
                if (req.length==0) {	//用户验证失败
                	alert("用户名或密码错误！");
                }else{
                	sessionStorage.setItem('username', req[0].username);
                	sessionStorage.setItem('user_type',req[0].user_type);
                	sessionStorage.setItem('user_Id',req[0].Id);
                	window.location.href = "index.php";
                }
            },
            error: function () {
                alert("数据接口请求错误！");
            }
 
        });
}
//处理用户注册
function DoRegest (){ 
	if (
        		$("#username").val()==""||
				$("#sex").val()==""||
				$("#password").val()==""||
				$("#Email").val()==""||
				$("#phone").val()==""||
				$("#address").val()=="" 
        		) 
        	{
        		alert("请填写所有内容！");
        		return;
        	}else{
        		$.ajax({
		            url: "action/AddUser.php", 
		            //dataType: "json", 
		            async: true,
		            data: $('#RegestForm').serialize(),//将比表单的值序列化作为参数提交 
		            type: "POST", //请求方式
		            success: function (req) {
		                if (req==200) {	//用户验证失败
		                	alert("注册成功！");
		                }else{
		                	alert("注册失败！");
		                }
		            },
		            error: function () {
		                alert("数据接口请求错误！");
		            }
		 
		        });
        	}
}
</script>
</body>
</html>