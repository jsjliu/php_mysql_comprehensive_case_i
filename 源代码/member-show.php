<!DOCTYPE html>
<html>
    
    <head>
        <meta charset="utf-8">
        <title>
            详情
        </title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="format-detection" content="telephone=no">
        <link rel="stylesheet" href="./css/x-admin.css" media="all">
        <script type="text/javascript" src="loginjs/jquery-1.11.1.min.js"></script>
    </head>
    
    <body>
        <div class="x-body">
            <blockquote class="layui-elem-quote">
                <dl style="margin-left:10px; color:#019688">
                <dt><span id="username"></span></dt>
              </dl>
            </blockquote>
            <div class="pd-20">
              <table  class="layui-table" lay-skin="line">
                <tbody>
                  <tr>
                    <th  width="80">密码：</th>
                    <td id="password"></td>
                  </tr>
                  <tr>
                    <th>性别：</th>
                    <td id="sex"></td>
                  </tr>
                  <tr>
                    <th>手机：</th>
                    <td id="phone"></td>
                  </tr>
                  <tr>
                    <th>邮箱：</th>
                    <td id="email"></td>
                  </tr>
                  <tr>
                    <th>地址：</th>
                    <td id="address"></td>
                  </tr>
                  <tr>
                    <th>加入时间：</th>
                    <td id="CreateTime"></td>
                  </tr>
                </tbody>
              </table>
            </div>
        </div>
        <script src="./lib/layui/layui.js" charset="utf-8">
        </script>
        <script src="./js/x-layui.js" charset="utf-8">
        </script>
        <script type="text/javascript">
        function getQueryString(name) {
               var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
               var r = window.location.search.substr(1).match(reg);
               if(r != null) {
                     return decodeURIComponent(r[2]);
               }
               return '';
        }
        var Id=getQueryString("Id");
          $.ajax({
                        url: "action/GetUserById.php", 
                        dataType: "json", 
                        async: true,
                        data:{
                            Id:Id
                        },
                        type: "POST", //请求方式
                        success: function (req) {
                           if (req.length>0) {
                            $("#username").html(req[0].username);
                            $("#password").html(req[0].password);
                            $("#sex").html(req[0].sex);
                            $("#email").html(req[0].email);
                            $("#phone").html(req[0].phone_number);
                            $("#CreateTime").html(req[0].create_time);
                            $("#address").html(req[0].address);
                           }
                        },
                        error: function () {
                            alert("数据接口请求错误！");
                        }
             
                    });
        </script>
    </body>

</html>