<!DOCTYPE html>
<html>
    
    <head>
        <meta charset="utf-8">
        <title>
            X-admin v1.0
        </title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="format-detection" content="telephone=no">
        <link rel="stylesheet" href="./css/x-admin.css" media="all">
        <script type="text/javascript" src="loginjs/jquery-1.11.1.min.js"></script>
    </head>
    
    <body>
        <div class="x-body">
            <form class="layui-form" id="EditFrom">
                <div class="layui-form-item">
                    <label for="Id" class="layui-form-label">
                        Id
                    </label>
                    <div class="layui-input-inline">
                        <input readonly="true" type="text" id="Id" name="Id" class="layui-input">
                    </div>

                    <label for="username" class="layui-form-label">
                        用户名
                    </label>
                    <div class="layui-input-inline">
                        <input type="text" id="username" name="username" class="layui-input">
                    </div>

                    <label for="password" class="layui-form-label">
                        密码
                    </label>
                    <div class="layui-input-inline">
                        <input type="text" id="password" name="password" class="layui-input">
                    </div>

                     <label for="sex" class="layui-form-label">
                        性别
                    </label>
                    <div class="layui-input-inline">
                        <input type="text" id="sex" name="sex" class="layui-input">
                    </div>

                    <label for="phone" class="layui-form-label">
                        手机
                    </label>
                    <div class="layui-input-inline">
                        <input type="text" id="phone" name="phone" class="layui-input">
                    </div>

                    <label for="email" class="layui-form-label">
                        邮箱
                    </label>
                    <div class="layui-input-inline">
                        <input type="text" id="email" name="email" class="layui-input">
                    </div>

                    <label for="address" class="layui-form-label">
                        地址
                    </label>
                    <div class="layui-input-inline">
                        <input type="text" id="address" name="address" class="layui-input">
                    </div>

                <div class="layui-form-item">
                    <label for="L_sign" class="layui-form-label">
                    </label>
                    <button class="layui-btn" key="set-mine" lay-filter="save" onclick="save()">
                        保存
                    </button>
                </div>
            </form>
        </div>
        <script src="./lib/layui/layui.js" charset="utf-8">
        </script>
        <script src="./js/x-layui.js" charset="utf-8">
        </script>
        <script>
        function getQueryString(name) {
               var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
               var r = window.location.search.substr(1).match(reg);
               if(r != null) {
                     return decodeURIComponent(r[2]);
               }
               return '';
        }
        var Id=getQueryString("Id");
          $.ajax({
                        url: "action/GetUserById.php", 
                        dataType: "json", 
                        async: true,
                        data:{
                            Id:Id
                        },
                        type: "POST", //请求方式
                        success: function (req) {
                           if (req.length>0) {
                            $("#Id").val(req[0].Id);
                            $("#username").val(req[0].username);
                            $("#password").val(req[0].password);
                            $("#sex").val(req[0].sex);
                            $("#email").val(req[0].email);
                            $("#phone").val(req[0].phone_number);
                            $("#address").val(req[0].address);
                           }
                        },
                        error: function () {
                            alert("数据接口请求错误！");
                        }
             
                    });

          function save(){
            //alert($('#EditFrom').serialize());
            $.ajax({
                    url: "action/UpdateUserById.php", 
                    //dataType: "json", 
                    async: true,
                    data: $('#EditFrom').serialize(),//将表单的值序列化作为参数提交 
                    type: "POST", //请求方式
                    success: function (req) {
                        if (req=='200') {
                            alert("已保存！");
                            var index = parent.layer.getFrameIndex(window.name); //获取子页面索引
                            parent.layer.close(index);//关闭子页面
                            parent.location.reload();//刷新父页面
                        }else{
                            alert("保存失败！");
                        }
                    },
                    error: function () {
                        alert("数据接口请求错误！");
                    }
         
                });
          }
        </script>
    </body>

</html>