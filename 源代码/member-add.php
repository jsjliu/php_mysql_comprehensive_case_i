<!DOCTYPE html>
<html>
    
    <head>
        <meta charset="utf-8">
        <title>
            用户管理
        </title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="format-detection" content="telephone=no">
        <link rel="stylesheet" href="./css/x-admin.css" media="all">
        <script type="text/javascript" src="loginjs/jquery-1.11.1.min.js"></script>
    </head>
    
    <body>
        <div class="x-body">
            <form class="layui-form" id="Form_AddUser">
                <div class="layui-form-item">
                    <label for="L_username" class="layui-form-label">
                        <span class="x-red">*</span>用户名
                    </label>
                    <div class="layui-input-inline">
                        <input type="text" id="L_username" name="username"
                        autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="L_sex" class="layui-form-label">
                        <span class="x-red">*</span>性别
                    </label>
                    <div class="layui-input-inline">
                        <input type="text" id="L_sex" name="sex"
                        autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="L_address" class="layui-form-label">
                        <span class="x-red">*</span>地址
                    </label>
                    <div class="layui-input-inline">
                        <input type="text" id="L_address" name="address"
                        autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="L_password" class="layui-form-label">
                        <span class="x-red">*</span>密码
                    </label>
                    <div class="layui-input-inline">
                        <input type="text" id="L_password" name="password"
                        autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="L_Email" class="layui-form-label">
                        <span class="x-red">*</span>Email
                    </label>
                    <div class="layui-input-inline">
                        <input type="Email" id="L_Email" name="Email"
                        autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="L_phone" class="layui-form-label">
                        <span class="x-red">*</span>电话
                    </label>
                    <div class="layui-input-inline">
                        <input type="text" id="L_phone" name="phone"
                        autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="L_repass" class="layui-form-label">
                    </label>
                    <button onclick="AddUser()" class="layui-btn" lay-filter="add" lay-submit="">
                        增加
                    </button>
                </div>
            </form>
        </div>
        <script src="./lib/layui/layui.js" charset="utf-8">
        </script>
        <script src="./js/x-layui.js" charset="utf-8">
        </script>
        <script>
        var _hmt = _hmt || [];
        (function() {
          var hm = document.createElement("script");
          hm.src = "https://hm.baidu.com/hm.js?b393d153aeb26b46e9431fabaf0f6190";
          var s = document.getElementsByTagName("script")[0]; 
          s.parentNode.insertBefore(hm, s);
        })();
        //检验数据有效性
        function AddUser(){
        	if (
        		$("#L_username").val()==""||
				$("#L_sex").val()==""||
				$("#L_address").val()==""||
				$("#L_password").val()==""||
				$("#L_Email").val()==""||
				$("#L_phone").val()=="" 
        		) 
        	{
        		alert("请填写必填内容！");
        		return;
        	}else{
        		DoAddUser ();
        	}
        }
        //新增用户
		function DoAddUser (){ 
			$.ajax({
		            url: "action/AddUser.php", 
		            //dataType: "json", 
		            async: true,
		            data: $('#Form_AddUser').serialize(),//将比表单的值序列化作为参数提交 
		            type: "POST", //请求方式
		            success: function (req) {
		            	if (req=='200') {
		            		alert("成功添加新用户！");
		            		var index = parent.layer.getFrameIndex(window.name); //获取子页面索引
                            parent.layer.close(index);//关闭子页面
                            parent.location.reload();//刷新父页面
		            	}else{
		            		alert("添加失败！");
		            	}
		            },
		            error: function () {
		                alert("数据接口请求错误！");
		            }
		 
		        });
		}
        </script>
    </body>

</html>