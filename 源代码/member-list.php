<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>
            用户管理
        </title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="format-detection" content="telephone=no">
        <link rel="stylesheet" href="./css/x-admin.css" media="all">
        <link rel="stylesheet" type="text/css" href="Pagecss/zxf_page.css"/>
        <script type="text/javascript" src="loginjs/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="Pagejs/zxf_page.js"></script>
    </head>
    <body>
        <div class="x-nav">
            <span class="layui-breadcrumb">
              <a><cite>首页</cite></a>
              <a><cite>用户管理</cite></a>
              <a><cite>用户列表</cite></a>
            </span>
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right"  href="javascript:location.replace(location.href);" title="刷新"><i class="layui-icon" style="line-height:30px">ဂ</i></a>
        </div>
        <div class="x-body">
                <div class="layui-form-pane" style="margin-top: 15px;">
                  <div class="layui-form-item">
                    <div class="layui-input-inline">
                      <input type="text" id="usernameKey"  placeholder="请输入用户名" autocomplete="off" class="layui-input">
                    </div>
                    <div class="layui-input-inline" style="width:80px">
                        <button class="layui-btn"  onclick="srarchByKey()" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
                    </div>
                  </div>
                </div> 
            <xblock>
                <button class="layui-btn" onclick="member_add('添加用户','member-add.php','600','500')"><i class="layui-icon">&#xe608;</i>添加</button><span class="x-right" style="line-height:40px">共有数据：<span id="usertotal"></span> 条</span></xblock>
            <table class="layui-table" id="dataTable">
                <thead>
                    <tr>
                        <th>
                            ID
                        </th>
                        <th>
                            用户名
                        </th>
                        <th>
                            密码
                        </th>
                        <th>
                            性别
                        </th>
                        <th>
                            手机
                        </th>
                        <th>
                            邮箱
                        </th>
                        <th>
                            地址
                        </th>
                        <th>
                            加入时间
                        </th>
                        <th>
                            操作
                        </th>
                    </tr>
                </thead>
                <tbody id="UserTB">
                    <!-- 循环内容体 -->
                </tbody>
            </table>
            <input style="display: none" type="text" id="Total" name="">
            <div class="zxf_pagediv"></div>
        </div>
        <script src="./lib/layui/layui.js" charset="utf-8"></script>
        <script src="./js/x-layui.js" charset="utf-8"></script>
        <script type="text/javascript">
        var PAGESIZE=5;//每页显示几条记录
        setTimeout(function(){
            var TOTAL=$("#Total").val();//获取总记录数
            $(".zxf_pagediv").createPage({
                pageNum: Math.ceil(TOTAL/PAGESIZE),//总记录/每页显示数=总页数
                current: 1,//当前页数
                backfun: function(e) {
                    PageInitForGetUser(e.current,PAGESIZE);//回调函数，根据页数和页大小重新取数
                }
            });
        },1000);
        </script>
        <script>
            layui.use(['laydate','element','laypage','layer'], function(){
                $ = layui.jquery;//jquery
              laydate = layui.laydate;//日期插件
              lement = layui.element();//面包导航
              layer = layui.layer;//弹出层
            });
             /*用户-添加*/
            function member_add(title,url,w,h){
                x_admin_show(title,url,w,h);
            }
        </script>
        <script type="text/javascript">
            //获取所有用户信息
            function PageInitForGetUser (page,pagesize) {
                $.ajax({
                        url: "action/GetAllUser.php", 
                        dataType: "json", 
                        async: true,
                        data:{
                            page:page,
                            pagesize:pagesize
                        },
                        type: "POST", //请求方式
                        success: function (req) {
                            HtmlStr="";
                            if (req.length>0) {
                                for (var i = 0; i <=req.length - 1; i++) {
                                    HtmlStr+='<tr><td>'+req[i].Id+'</td><td><u style="cursor:pointer" onclick="Show('+req[i].Id+')">'+req[i].username+'</u></td><td >'+req[i].password+'</td><td >'+req[i].sex+'</td><td >'+req[i].phone_number+'</td><td >'+req[i].email+'</td><td >'+req[i].address+'</td><td>   '+req[i].create_time+'</td><td class="td-manage"><a title="编辑" href="javascript:;" onclick="Edit('+req[i].Id+')"class="ml-5" style="text-decoration:none"><i class="layui-icon">&#xe642;</i></a><a title="删除" href="javascript:;" onclick="Delete('+req[i].Id+')" style="text-decoration:none"><i class="layui-icon">&#xe640;</i></a></td> </tr>';
                                }
                                $("#UserTB").html(HtmlStr);
                            }
                        },
                        error: function () {
                            alert("数据接口请求错误！");
                        }
             
                    });
            }
            //setInterval("PageInitForGetUser()",2000);
            PageInitForGetUser(1,PAGESIZE);//默认只加载第一页
            function GetUserTotal(){
                $.ajax({
                        url: "action/GetUserTotal.php", 
                        //dataType: "json", 
                        async: true,
                        type: "POST", //请求方式
                        success: function (req) {
                            $("#usertotal").html(req);
                            $("#Total").val(req);
                        },
                        error: function () {
                            alert("数据接口请求错误！");
                        }
             
                    });
            }
            GetUserTotal();
            //查看详情
            function Show(Id){
                x_admin_show("详情","member-show.php?Id="+Id,400,600);
            }
            //编辑用户
            function Edit(Id){
                x_admin_show("编辑","member-edit.php?Id="+Id,400,600);
            }
            //删除用户
            function Delete(Id){
                $.ajax({
                        url: "action/DelUserById.php", 
                        //dataType: "json", 
                        async: true,
                        data:{
                            Id:Id
                        },
                        type: "POST", //请求方式
                        success: function (req) {
                           if (req=='200') {
                            alert("已删除！");
                            location.reload();//刷新当前页面
                        }else{
                            alert("删除失败！");
                        }
                        },
                        error: function () {
                            alert("数据接口请求错误！");
                        }
             
                    });
            }
            function srarchByKey(){
                if ($("#usernameKey").val()=="") {
                    return;
                }
                else{
                    window.location.href="member-listByKey.php?Key="+$("#usernameKey").val();
                }
            }
        </script>
    </body>
</html>